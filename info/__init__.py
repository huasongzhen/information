from flask import Flask
# from flask.ext.migrate import Migrate, MigrateCommand
# from flask.ext.script import Manager
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
import redis
from flask_wtf.csrf import CSRFProtect
from config import Config, DevevelopementConfig, ProductionConfig

# app = Flask(__name__)
#
#
# app.config.from_object(Config)
# #配置数据库
db = SQLAlchemy()
redis_store = None
# # 配置redis
# redis_store = redis.StrictRedis(host=Config.REDIS_HOST,port=Config.REDIS_PORT)
# # 开启csrf保护
# CSRFProtect(app)
# #设置session保存位置
# Session(app)
# manager =Manager(app)

# Migrate(app,db)
# manager.add_command('db',MigrateCommand)

config = {
    'development': DevevelopementConfig,
    'production': ProductionConfig
}


def create_app(config_name):
    """通过传入不同的配置名字，初始化其对应配置的应用实例"""
    app = Flask(__name__)

    app.config.from_object(config[config_name])
    # 配置数据库
    # db = SQLAlchemy(app)
    db.init_app(app)
    # 配置redis
    global redis_store
    redis_store = redis.StrictRedis(host=config[config_name].REDIS_HOST, port=config[config_name].REDIS_PORT)
    # 开启csrf保护
    CSRFProtect(app)
    # 设置session保存位置
    Session(app)

    from info.modules.index import index_blu

    app.register_blueprint(index_blu)

    return app
