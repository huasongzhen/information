# from flask import Flask
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager
# from flask_session import Session
# from flask_sqlalchemy import SQLAlchemy
# import redis
# from flask_wtf.csrf import CSRFProtect
# from config import Config
from info import create_app, db

#
app = create_app('development')
# app = Flask(__name__)

#
# class Config(object):
#     """工程配置信息"""
#     SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"
#
#     DEBUG = True
#     # 数据库的配置信息
#     SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/information'
#     SQLALCHEMY_TRACK_MODIFIATIONS = False
#     #redis配置
#     REDIS_HOST = '127.0.0.1'
#     REDIS_PORT = 6379
#     #flask_session的配置信息
#     SESSION_TYPE = 'redis' #指定 session 保存到 redis中
#     SESSION_USE_SIGNER = True #让 cookie 中的 session_id 被加密签名处理
#     #使用redis 的实例
#     SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
#     PERMANENT_SESSION_LIFETIME = 86400 # session 的有效期，单位：秒
#
# app.config.from_object(Config)
#
# db = SQLAlchemy(app)
#
# CSRFProtect(app)
#
manager = Manager(app)
# 数据库 迁移
Migrate(app, db)
manager.add_command('db', MigrateCommand)

#
# @app.route('/')
# def index():
#     return 'hello index'


if __name__ == '__main__':
    manager.run()
